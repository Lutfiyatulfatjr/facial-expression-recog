# USAGE
# python detect_face_parts.py --shape-predictor shape_predictor_68_face_landmarks.dat --image images/example_01.jpg 

# import the necessary packages
from imutils import face_utils
import numpy as np
import argparse
import imutils
import dlib
import cv2
import csv

# initialize dlib's face detector (HOG-based) and then create
# the facial landmark predictor
detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor('shape_predictor_68_face_landmarks.dat')



# load the input image, resize it, and convert it to grayscale
image = cv2.imread('coba.JPG')
image = imutils.resize(image, width=500)
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
	# detect faces in the grayscale image
rects = detector(gray, 1)
for (i, rect) in enumerate(rects):
    shape = predictor(gray, rect)
    shape = face_utils.shape_to_np(shape)
    feature = []
    (x, y, w, h) = face_utils.rect_to_bb(rect)
    cv2.rectangle(image, (x, y), (x + w, y + h), (0, 0, 255), 2)
    cv2.putText(image, "Face #{}".format(i + 1), (x - 10, y - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
    for (name, (i,j)) in face_utils.FACIAL_LANDMARKS_IDXS.items():
        for (x, y) in shape[i:j]:
            if name == "mouth":
                cv2.circle(image, (x, y), 1, (0, 255, 255), -1)
                print("Mulut : ")
                print(x,y)
            elif name == "left_eye":
                cv2.circle(image, (x, y), 1, (0, 255, 255), -1)
                print("Mata kiri : ")
                print(x,y)
            elif name == "right_eye":
                cv2.circle(image, (x, y), 1, (0, 255, 255), -1)
                print("Mata Kanan : ")
                print(x,y)
            elif name == "left_eyebrow":
                cv2.circle(image, (x, y), 1, (0, 255, 255), -1)
                print("Alis kiri : ")
                print(x,y)
            elif name == "right_eyebrow":
                cv2.circle(image, (x, y), 1, (0, 255, 255), -1)
                print("Alis Kanan : ")
                print(x,y)
cv2.imwrite('hasil.jpg', image)
cv2.imshow("Output", image)
cv2.waitKey(0)