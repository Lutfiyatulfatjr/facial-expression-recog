import pymysql
import os

class Database(object):
    def connect():
        connection = pymysql.connect(
            host='localhost',
            user='root',
            password='',
            db='ta',
        )
        return connection

    def insert(db, photos_name, olshop_name):
        try:
            with db.cursor() as cursor:
                sql = 'INSERT INTO photos (photos_name, olshop_name, id_olshop) VALUES (%s,%s,  (SELECT id FROM olshop WHERE olshop_name =%s))'
                try:
                    cursor.execute(sql, (photos_name, olshop_name, olshop_name))
                    print("Insert success")
                except:
                    print('Oops! something wrong')
            db.commit()
        except:
            print('Err')
    
    def insertOlshop(db, olshop_name):
        try:
            with db.cursor() as cursor:
                sql = "INSERT INTO olshop (olshop_name) SELECT * FROM (SELECT %s) AS tmp WHERE NOT EXISTS (SELECT olshop_name FROM olshop WHERE olshop_name = %s) LIMIT 1"
                try:
                    cursor.execute(sql, (olshop_name, olshop_name))
                    print("Insert success")
                except:
                    print('Oops! something wrong')
            db.commit()
        except:
            print('Errorrrr')
    
    def check_user(db, username):
        try:
            with db.cursor() as cursor:
                sql = "SELECT count(*) FROM user WHERE username = %s"
                try:
                    cursor.execute(sql, (username))
                    result = cursor.fetchone()
                    if result[0] == 1:
                        state=1
                    else:
                        state=0
                    return state
                except:
                    print('Oops! something wrong')
            db.commit()
        except:
            print('Errorrrr')

    def sign_up(db, username, password):
        try:
            with db.cursor() as cursor:
                sql="INSERT into user (username, password) values (%s, %s)"
                try:
                    cursor.execute(sql, (username, password))
                    print('insert success')
                except:
                    print('Oops')
            db.commit()
        except:
            print('errrorrrr')

    def login(db, username, password):
        try:
            with db.cursor() as cursor:
                sql = "SELECT * FROM user WHERE username = %s and password=%s"
                try:
                    cursor.execute(sql, (username, password))
                    result = cursor.fetchone()
                    print(result)
                    if result[1] == username:
                        state=1
                    else:
                        state=0
                    return state, result
                except:
                    print('Oops! something wrong')
            db.commit()
        except:
            print('Errorrrr')
    
    def inserthasil(db, id_user, date, time, olshop_name):
        try:
            with db.cursor() as cursor:
                sql = 'INSERT INTO detection (id_user, date,time, id_olshop) VALUES (%s, %s, %s, (SELECT id FROM olshop WHERE olshop_name =%s))'
                try:
                    cursor.execute(sql, (id_user, date, time, olshop_name))
                    print("Insert success")
                except:
                    print('Oops! something wrong')
            db.commit()
        except:
            print('Err')
    
    def insertdetail(db, time, photos_name, expression, experiment, id_user):
        print(photos_name)
        print(time)
        print(experiment)
        print(expression)
        print(id_user)
        try:
            with db.cursor() as cursor:
                sql = 'INSERT INTO detection_detail (id_detection, time, id_photos, expression, experiment) VALUES ((SELECT MAX(id) FROM detection where id_user=%s), %s, (SELECT id FROM photos WHERE photos_name = %s), %s, %s)'
                try:
                    cursor.execute(sql, (id_user, time, photos_name, expression, experiment))
                    print("Insert success")
                except:
                    print('Oops! something wrong')
            db.commit()
        except:
            print('Err')
    
    def mostliked(db):
        try:
            with db.cursor() as cursor:
                sql = 'SELECT photos_name, olshop_name, id_photos, count(id_detection) from photos, detection_detail, detection where photos.id = detection_detail.id_photos and detection.id = detection_detail.id_detection and expression=%s group by olshop_name, photos_name order by count(id_detection) desc'
                try: 
                    cursor.execute(sql,('suka'))
                    mostliked = cursor.fetchall()
                    return mostliked
                except:
                    print('Oopsss')
            db.commit()
        except:
            print('Err')
    
    def mostviewed(db):
        try:
            with db.cursor() as cursor:
                sql = 'SELECT  photos_name, olshop_name, id_photos, count(id_photos) from photos, detection_detail, detection where photos.id = detection_detail.id_photos and detection.id = detection_detail.id_detection group by olshop_name, photos_name order by count(id_photos) desc'
                try:
                    cursor.execute(sql)
                    mostviewed = cursor.fetchall()
                    return mostviewed
                except:
                    print('Oops')
            db.commit()
        except:
            print('Errrr')
    
    def expresult(db, id_user):
        try:
            with db.cursor() as cursor:
                sql = 'SELECT detection.id, date, olshop_name from detection, olshop where id_user=%s and olshop.id=detection.id_olshop'
                try:
                    cursor.execute(sql, (id_user))
                    expresult = cursor.fetchall()
                    return expresult
                except:
                    print('Oops')
        except:
            print('error')
    
    def result_detail(db, id_detection, experiment):
        print(id_detection)
        print(experiment)
        try:
            with db.cursor() as cursor:
                sql = 'SELECT id_photos, expression FROM detection_detail WHERE id_detection=%s and experiment=%s'
                try:
                    cursor.execute(sql, (id_detection, experiment))
                    result_detail = cursor.fetchall()
                    print(result_detail)
                    return result_detail
                except:
                    print('Oops')
        except:
            print('error')
    
    def likedbyuser(db, id):
        try:
            with db.cursor() as cursor:
                sql='SELECT photos.photos_name,photos.olshop_name, expression, count(expression) FROM detection_detail, photos, detection WHERE expression=%s and id_detection=%s and photos.id=detection_detail.id_photos and photos.id_olshop=detection.id_olshop group by id_photos order by count(expression) desc'
                try:
                    cursor.execute(sql, ('suka', id))
                    likedbyuser = cursor.fetchall()
                    return likedbyuser
                except:
                    print('Oops')
        except:
            print('Error')
            
    def alluser(db):
        try:
            with db.cursor() as cursor:
                sql='SELECT * FROM `user` WHERE username != "admin"'
                try:
                    cursor.execute(sql)
                    alluser = cursor.fetchall()
                    return alluser
                except:
                    print('Oops')
        except:
            print('Error')
    
    def edituser(db, username, birthday, occupation):
        try:
            with db.cursor() as cursor:
                sql='UPDATE user SET username=%s, birthday = %s, occupation = %s WHERE user.username=%s'
                try:
                    cursor.execute(sql, (username, birthday, occupation, username))
                    message="insert success"
                    return message
                except:
                    print('Oops')
        except:
            print('Error')
    
    def listdetection(db):
        try:
            with db.cursor() as cursor:
                sql='SELECT detection.id, user.username, detection.date, detection.time, olshop.olshop_name from detection, user, olshop where detection.id_user=user.id and detection.id_olshop=olshop.id'
                try:
                    cursor.execute(sql)
                    listdetection=cursor.fetchall()
                    return listdetection
                except:
                    print('Oops')
        except:
            print('Error')
    
    def listolshop(db):
        try:
            with db.cursor() as cursor:
                sql='SELECT olshop.id, olshop.olshop_name, count(photos.photos_name) from olshop, photos where photos.id_olshop=olshop.id group by olshop.olshop_name'
                try:
                    cursor.execute(sql)
                    listolshop=cursor.fetchall()
                    return listolshop
                except:
                    print('Oops')
        except:
            print('Error')
    
    def olshop_id(db):
        try:
            with db.cursor() as cursor:
                sql='SELECT olshop.id from olshop'
                try:
                    cursor.execute(sql)
                    olshop_id=cursor.fetchall()
                    return olshop_id
                except:
                    print('Oops')
        except:
            print('Error')
    
    def detectionbyuser(db):
        try:
            with db.cursor() as cursor:
                sql='SELECT user.username, count(*) FROM detection, user where user.id=detection.id_user group by id_user'
                try:
                    cursor.execute(sql)
                    detectionbyuser=cursor.fetchall()
                    return detectionbyuser
                except:
                    print('Oops')
        except:
            print('Error')
    
    def detectionbyolshop(db):
        try:
            with db.cursor() as cursor:
                sql='SELECT olshop.olshop_name, COUNT(*) FROM detection, olshop where olshop.id=detection.id_olshop group by id_olshop'
                try:
                    cursor.execute(sql)
                    detectionbyolshop=cursor.fetchall()
                    return detectionbyolshop
                except:
                    print('Oops')
        except:
            print('Error')
    
    def detectionbydate(db):
        try:
            with db.cursor() as cursor:
                sql='SELECT detection.date, count(*) FROM detection group by detection.date'
                try:
                    cursor.execute(sql)
                    detectionbydate=cursor.fetchall()
                    return detectionbydate
                except:
                    print('Oops')
        except:
            print('Error')
    
    def olshopreport(db, id):
        try:
            with db.cursor() as cursor:
                sql='SELECT p.photos_name, COUNT(dd.id_photos) AS jumlah FROM detection_detail dd, detection d, photos p WHERE dd.id_detection = d.id AND d.id_olshop = (select id from olshop where olshop.olshop_name=%s) AND dd.id_photos=p.id GROUP BY dd.id_photos ORDER BY dd.id_photos ASC'
                try:
                    cursor.execute(sql,(id))
                    olshopreport=cursor.fetchall()
                    return olshopreport
                except:
                    print('Oops')
        except:
            print('Error')
    
    def photobyuser(db,id_user, experiment):
        print(id_user)
        try:
            with db.cursor() as cursor:
                sql='SELECT id_photos, expression FROM detection_detail WHERE id_detection=(SELECT MAX(id) FROM detection where detection.id_user=%s) and experiment=%s'
                try:
                    cursor.execute(sql, (id_user, experiment))
                    photobyuser=cursor.fetchall()
                    return photobyuser
                except:
                    print('Oops')
        except:
            print('Error')
    
    def lateslike(db,id_user):
        try:
            with db.cursor() as cursor:
                sql='SELECT detection_detail.id_photos, photos.photos_name,photos.olshop_name, expression, count(expression) FROM detection_detail, photos, detection WHERE expression=%s and id_detection=(SELECT MAX(id) FROM detection where detection.id_user=%s) and photos.id=detection_detail.id_photos and photos.id_olshop=detection.id_olshop group by id_photos order by count(expression) desc'
                try:
                    cursor.execute(sql, ('suka', id_user))
                    lateslike=cursor.fetchall()
                    return lateslike
                except:
                    print('Oops')
        except:
            print('Error')
    
    def getiduser(db,username):
        try:
            with db.cursor() as cursor:
                sql='SELECT id from user where username=%s'
                try:
                    cursor.execute(sql, (username))
                    getiduser=cursor.fetchall()
                    return getiduser
                except:
                    print('Oops')
        except:
            print('Error')
    
    def likedproducts(db, id_user):
        try:
            with db.cursor() as cursor:
                sql='SELECT photos.photos_name,photos.olshop_name, expression, count(expression) FROM detection_detail, photos, detection WHERE expression=%s and detection.id_user=%s and photos.id=detection_detail.id_photos and photos.id_olshop=detection.id_olshop group by id_photos order by photos.olshop_name desc'
                try:
                    cursor.execute(sql, ('suka', id_user))
                    getlikedproducts=cursor.fetchall()
                    return getlikedproducts
                except:
                    print('Oops')
        except:
            print('Error')