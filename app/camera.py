from flask import Flask
from app import app
from time import time, strftime
from datetime import datetime
from imutils.video import VideoStream
from imutils import face_utils
import datetime
import argparse
import imutils
import time
import dlib
import cv2
import csv
import math
import json
import requests

detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor('shape_predictor_68_face_landmarks.dat')

class VideoCamera(object):
    def __init__(self):
        # Using OpenCV to capture from device 0. If you have trouble capturing
        # from a webcam, comment the line below out and use a video file
        # instead.
        self.video = cv2.VideoCapture(0)
        # If you decide to use video.mp4, you must have this file in the folder
        # as the main.py.
        # self.video = cv2.VideoCapture('video.mp4')
    
    def __del__(self):
        self.video.release()

    def hitungNormalisasi(self, feature):
        newmaks_x=1
        newmaks_y = 1
        newmin_x=0
        newmin_y = 0
        minimal_y=1000
        minimal_x = 1000
        maks_x=0
        maks_y = 0
        for b in range(len(feature)):
            if (b % 2 == 0):
                if (feature[b] < minimal_x):
                    minimal_x = float(feature[b])
                if (feature[b] > maks_x):
                    maks_x = float(feature[b])
            else:
                if (feature[b] < minimal_y):
                    minimal_y = float(feature[b])
                if (feature[b] > maks_y):
                    maks_y = float(feature[b])
        
        for c in range(len(feature)):
            if (c%2 == 0):
                newdata = (float(feature[c])-minimal_x) * (newmaks_x - newmin_x) / (maks_x - minimal_x) + newmin_x
                feature[c] = newdata
            else:
                newdata = (float(feature[c])-minimal_y) * (newmaks_y - newmin_y) / (maks_y - minimal_y) + newmin_y
                feature[c] = newdata
        
        return feature
    
    def knn(self, feature, startTim):

        with open('fitur.csv', 'r') as f:
            reader = csv.reader(f)
            mylist = list(reader)

        hasil = list()
        now = datetime.datetime.now() 
        waktu = now - startTim
        #print(waktu)
        
        

        #KNN
        for i in range(len(mylist)):
            temp = []
            tmp = 0
            length = len(mylist[i]) - 1
            for j in range(length):
                data = (float(feature[j]) - float(mylist[i][j]))**2
                tmp += data
            hsl = math.sqrt(tmp)
            temp.append(hsl)
            temp.append(mylist[i][84])
            hasil.append(temp)
        
        sorthasil = sorted(range(len(hasil)), key=lambda k:hasil[k])
        tidak_suka=0
        suka= 0
        jml = 0

        time_expression = list()
        time_expression.append(waktu)

        neighbor = 5
        for i in range (0,neighbor):
            if hasil[sorthasil[i]][1] == 'tidak suka':
                tidak_suka += 1
            elif hasil[sorthasil[i]][1] == 'suka':
                suka += 1

        if tidak_suka > suka:
            expression='tidak suka'
            jml = tidak_suka
            #print('tidak suka = %d dari 5 ' % tidak_suka)
            time_expression.append(expression)
            time_expression.append(jml)
            time_expression.append(neighbor)
            #requests.get("http://localhost:3000/kirimData?data="+json.dumps(time_expression, indent=4, default=str)).json()

        elif suka > tidak_suka:
            expression = 'suka'
            jml = suka
            time_expression.append(expression)
            time_expression.append(jml)
            time_expression.append(neighbor)
            #requests.get("http://localhost:3000/kirimData?data="+json.dumps(time_expression, indent=4, default=str)).json()

        requests.get("http://localhost:3000/kirimData?data="+json.dumps(time_expression, indent=4, default=str)).json()

    def get_frame(self, startTim):
        success, frame = self.video.read()
        time.clock()
        if success == True:
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

            #detect face in grayscale frame
            rects = detector(gray,0)

            #variable to save expression
            expression = list()
            #loop over face detection
            for (i, rect) in enumerate(rects):
                # determine the facial landmarks for the face region, then
                # convert the facial landmark (x, y)-coordinates to a NumPy
                # array
                shape = predictor(gray, rect)
                shape = face_utils.shape_to_np(shape)
                feature = list()
                
                #loop over the (x,y)-coordinates for the facial landmarks
                #and draw them on the image
                for(name, (i,j)) in face_utils.FACIAL_LANDMARKS_IDXS.items():
                    for(x,y) in shape[i:j]:
                        if name == "mouth":
                            cv2.circle(frame, (x,y), 2, (255,255,255), -1)
                            feature.append(x)
                            feature.append(y)
                        elif name == "left_eyebrow":
                            cv2.circle(frame, (x,y), 2, (255,255,255), -1)
                            feature.append(x)
                            feature.append(y)
                        elif name == "right_eyebrow":
                            cv2.circle(frame, (x,y), 2, (255,255,255), -1)
                            feature.append(x)
                            feature.append(y)
                        elif name == "left_eye":
                            cv2.circle(frame, (x,y), 2, (255,255,255), -1)
                            feature.append(x)
                            feature.append(y)
                        elif name == "right_eye":
                            cv2.circle(frame, (x,y), 2, (255,255,255), -1)
                            feature.append(x)
                            feature.append(y)
                
                fitur = self.hitungNormalisasi(feature)
                
                self.knn(fitur, startTim)
                print('-------------------------')

                #save to note
                #result = {'time' : waktu, 'ekspresi' : expression, 'knn' : knn }

            # We are using Motion JPEG, but OpenCV defaults to capture raw images,
            # so we must encode it into JPEG in order to correctly display the
            # video stream.
            ret, jpeg = cv2.imencode('.jpg', frame)
            return jpeg.tobytes(), expression