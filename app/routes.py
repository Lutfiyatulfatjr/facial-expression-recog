from flask import render_template, Response, request, url_for, session, redirect, make_response, jsonify
from app import app, camera, instagram_scraper, database
from app.database import Database
from datetime import datetime
from app.camera import VideoCamera
from app.loginform import LoginForm
from imutils.video import VideoStream
from imutils import face_utils
import os, argparse, imutils, time, dlib, csv, cv2, subprocess, requests, json

@app.route('/')
@app.route('/index')
def index():
    db = Database.connect()
    mostliked = Database.mostliked(db)
    mostviewed = Database.mostviewed(db)
    if session.get('logged_in') == True:
        return render_template('index.html', statuslogin = 'ya' , username=session['username'], mostliked = mostliked, mostviewed = mostviewed)
    else:
        return render_template('index.html', statuslogin = 'tidak', mostliked = mostliked , mostviewed = mostviewed)

@app.route('/profile')
def profile():
    if "username" in session :
        db = Database.connect()
        id_user=Database.getiduser(db, session['username'])
        session['id_user']=id_user[0][0]
        expresult = Database.expresult(db, session['id_user'])
        likedbyuser = Database.likedbyuser(db, session['username'])
        lateslike= Database.lateslike(db, session['id_user'])
        lateslike_len = len(lateslike)
        return render_template('profile.html', username=session['username'], expresult = expresult, statuslogin='ya', likedbyuser=likedbyuser, lateslike=lateslike, lateslike_len=lateslike_len)

@app.route('/admin')
def admin():
    if "username" in session :
        db = Database.connect()
        alluser = Database.alluser(db)
        alldetection = Database.listdetection(db)
        allolshop = Database.listolshop(db)
        detectionbyuser = Database.detectionbyuser(db)
        return render_template('admin.html', statuslogin='ya', alluser=alluser, alldetection=alldetection, allolshop=allolshop, detectionbyuser=detectionbyuser)

@app.route('/likedproducts/ajax/') 
def likedproducts():
    db=Database.connect()
    id_detection = request.args.get('id')
    likedbyuser=Database.likedbyuser(db,id_detection)
    print(likedbyuser)
    return make_response(jsonify(likedbyuser), 200)

@app.route('/profile/ajax/')
def profile_ajax():
    id_detection = request.args.get('id')
    db = Database.connect()
    result_detail1= Database.result_detail(db, id_detection,1)
    result_detail2= Database.result_detail(db, id_detection,2)
    result_detail3= Database.result_detail(db, id_detection,3)
    likedbyuser=Database.likedbyuser(db,id_detection)
    return make_response(jsonify(result_detail1,result_detail2,result_detail3, likedbyuser), 200)

@app.route('/photobyuser/ajax/')
def photobyuser():
    id = request.args.get('id')
    db = Database.connect()
    photobyuser1= Database.photobyuser(db, session['id_user'],1)
    photobyuser2= Database.photobyuser(db, session['id_user'],2)
    photobyuser3= Database.photobyuser(db, session['id_user'],3)
    return make_response(jsonify(photobyuser1, photobyuser2, photobyuser3), 200)


@app.route('/detectionbyuser/ajax/')
def detectionbyuser():
    db = Database.connect()
    detectionbyuser= Database.detectionbyuser(db)
    return make_response(jsonify(detectionbyuser), 200)

@app.route('/detectionbyolshop/ajax/')
def detectionbyolshop():
    db = Database.connect()
    detectionbyolshop= Database.detectionbyolshop(db)
    return make_response(jsonify(detectionbyolshop), 200)

@app.route('/detectionbydate/ajax/')
def detectionbydate():
    db = Database.connect()
    detectionbydate= Database.detectionbydate(db)
    return make_response(jsonify(detectionbydate), 200)

@app.route('/hosofshopaholic/ajax/')
def hosofshopaholic():
    id = request.args.get('id')
    db = Database.connect()
    olshopreport= Database.olshopreport(db, id)
    return make_response(jsonify(olshopreport), 200)

@app.route('/cekhasil')
def cekhasil():
    return render_template('cekhasil.html')

@app.route('/edit')
def edit():
    username = request.args.get('username')
    birthday = request.args.get('birthday')
    occupation = request.args.get('occupation')
    print(username+""+birthday+""+occupation)
    db = Database.connect()
    message = Database.edituser(db, username, birthday, occupation)
    return make_response(jsonify(message), 200)

@app.route('/directory')
def directory():
    profpics = list()
    subdirs = os.listdir('app/static/fashion-photos/instagramscraper')
    for subdir in subdirs:
        img = os.listdir('app/static/fashion-photos/instagramscraper/'+subdir)[0]
        profpics.append(img)
    jml_dir = len(subdirs)
    if "username" not in session:
        return render_template('directory.html', subdirs = subdirs, profpics = profpics, jml_dir = jml_dir, statuslogin='tidak')
    else:
        return render_template('directory.html', subdirs = subdirs, profpics = profpics, jml_dir = jml_dir, statuslogin='ya')

@app.route('/photos')
def photos():
    dir_name = request.args.get('dir_name')
    images = list()
    for file in  os.listdir(os.path.join(app.static_folder+'/fashion-photos/instagramscraper', dir_name)):
        if file.endswith(".jpg"):
            images.append(file)
    if "username" not in session:
        return render_template('photos.html', images=images, dir_name = dir_name, statuslogin='tidak')
    else:
        return render_template('photos.html', images=images, dir_name = dir_name, statuslogin='ya')

def gen(camera):
    start = datetime.now()
    while True:
        frame, hasil = camera.get_frame(start)
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')

@app.route('/video_feed')
def video_feed():
    return Response(gen(VideoCamera()),mimetype='multipart/x-mixed-replace; boundary=frame')

@app.route('/stop')
def stop():
    VideoCamera.stop(camera)

@app.route('/slideshow')
def slideshow():
    dir_name = request.args.get('dir_name')
    images = os.listdir(os.path.join(app.static_folder+'/fashion-photos/instagramscraper', dir_name))
    if "username" not in session:
        return redirect(url_for('login'))
    else:
        return render_template('slideshow.html', dir_name = dir_name, images = images, statuslogin='ya')

@app.route('/search', methods=['POST', 'GET'])
def search():
    if "username" not in session:
        return redirect(url_for('index'))
    else:
        if request.method == 'POST':
            user_username = request.form['user_username']
            user_password = request.form['user_password']
            olshop_username = request.form['olshop_username']
            jml_foto = int(request.form['jml_foto'])
            bashCommand = "instagram-scraper %(os)s -m %(jml)d -d app/static/fashion-photos/instagramscraper/%(os)s -u %(us)s -p %(ps)s -t image " % {'os' : olshop_username, 'us': user_username, 'ps': user_password, 'jml' : jml_foto}
            process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
            output, error = process.communicate()
            olshop_name = olshop_username
            db= Database.connect()
            Database.insertOlshop(db, olshop_name)
            for file in  os.listdir(os.path.join(app.static_folder+'/fashion-photos/instagramscraper', olshop_name)):
                if file.endswith(".jpg"):
                    path, filename = os.path.split(file)
                    Database.insert(db, filename, olshop_name)
            return redirect(url_for('directory'))
        else:
            return render_template('search.html', statuslogin='ya')

@app.route('/hasil', methods=['POST', 'GET'])
def hasil():
    time_dict = {}
    pict_dict = {}
    experiment={}
    if request.method=="POST":
        hasil = request.form['data']
        parsed = json.loads(hasil)
        now= datetime.now()
        date = datetime.date(now)
        times = time.strftime("%H:%M:%S")
        db = Database.connect()
        Database.inserthasil(db, session['id_user'],date,times, parsed[0]['olshop_name'])
        for i in range (len(parsed)):
            key = parsed[i]['waktu'].split(".")
            sec = int(key[0])+1
            if sec not in time_dict :
                time_dict[sec]=[parsed[i]['ekspresi']]
                pict_dict[sec]=parsed[i]['idgambar']
                experiment[sec]=parsed[i]['experiment']
            else:
                time_dict[sec].append(parsed[i]['ekspresi'])
        for key, value in time_dict.items():
            suka=0
            tidak_suka = 0
            for v in range(len(value)):
                if value[v]=='suka':
                    suka+=1
                elif value[v] == 'tidak suka':
                    tidak_suka+=1
            if suka > tidak_suka:
                time_dict.update({key:'suka'})
            else:
                time_dict.update({key:'tidak suka'})
            Database.insertdetail(db, key, pict_dict[key], time_dict[key], experiment[key], session['id_user'])
        return redirect(url_for('profile'))
    else:
        if "username" not in session:
            return redirect(url_for('index'))
        else:
            return render_template('profile.html', parsed = parsed, time_dict = time_dict)


@app.route('/savedb')
def savedb():
    db = Database.connect()
    subdirs = os.listdir('app/static/fashion-photos/instagramscraper')
    dir_len = len(subdirs)
    for i in range(0, dir_len):
        Database.insertOlshop(db, subdirs[i])
        for file in  os.listdir(os.path.join(app.static_folder+'/fashion-photos/instagramscraper', subdirs[i])):
            if file.endswith(".jpg"):
                path, filename = os.path.split(file)
                Database.insert(db, filename, subdirs[i])
    return render_template('index.html')

@app.route('/deletedata')
def deletedata():
    db = Database.connect()
    Database.delete(db, photos)
    return render_template('index.html')

@app.route('/login', methods=['POST', 'GET'])
def login():
    db = Database.connect()
    if request.method=="POST":
        username = request.form['username']
        password = request.form['password']
        state, result = Database.login(db, username, password)
        if state == 1 and username == "admin":
            session['username'] = username
            session['id_user'] = result[0]
            return redirect(url_for('admin'))
        elif state == 1 and username != "admin":
            session['username'] = username
            session['id_user'] = result[0]
            session['user'] = result
            session['logged_in'] = True
            print(session['user'])
            return redirect(url_for('index'))
        else:
            loginstate='failed'
            return render_template('login.html', loginstate=loginstate, )
    else:
        return render_template('login.html')

@app.route('/sign_up', methods=['POST', 'GET'])
def sign_up():
    db = Database.connect()
    if request.method=="POST":
        username = request.form['username']
        password = request.form['password']
        checkuser = Database.check_user(db, username)
        if checkuser == 0:
            Database.sign_up(db, username, password)
            id_user=Database.getiduser(db, username)
            session['id_user']=id_user[0][0]
            session['username']=username
            session['logged_in'] = True
            return redirect(url_for('index'))
        else:
            state='failed'
            return render_template('sign_up.html', state=state)
    else:
        return render_template('sign_up.html')

@app.route('/logout', methods=['GET'])    
def logout():
    session.pop('username')
    session.pop('id_user')
    session.pop('logged_in', None)
    return redirect(url_for('index'))
    