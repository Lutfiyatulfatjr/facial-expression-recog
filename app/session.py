from flask import session

def check_login_session():
    if 'username' in session:
        return True
    else:
        return False