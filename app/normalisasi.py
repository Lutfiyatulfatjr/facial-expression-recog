from flask import Flask
from app import app
import math

class Normalisasi():
    def hitungNormalisasi(feature):
        newmaks_x=1
        newmaks_y = 1
        newmin_x=0
        newmin_y = 0
        minimal_y=1000
        minimal_x = 1000
        maks_x=0
        maks_y = 0
        for b in range(len(feature)):
            if (b % 2 == 0):
                if (feature[b] < minimal_x):
                    minimal_x = float(feature[b])
                if (feature[b] > maks_x):
                    maks_x = float(feature[b])
            else:
                if (feature[b] < minimal_y):
                    minimal_y = float(feature[b])
                if (feature[b] > maks_y):
                    maks_y = float(feature[b])
        
        for c in range(len(feature)):
            if (c%2 == 0):
                newdata = (float(feature[c])-minimal_x) * (newmaks_x - newmin_x) / (maks_x - minimal_x) + newmin_x
                feature[c] = newdata
            else:
                newdata = (float(feature[c])-minimal_y) * (newmaks_y - newmin_y) / (maks_y - minimal_y) + newmin_y
                feature[c] = newdata
        
        return feature