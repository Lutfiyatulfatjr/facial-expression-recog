from flask import Flask
app = Flask(__name__, static_url_path='/static')

from app import routes
from app import camera
from app import instagram_scraper
from app import loginform
