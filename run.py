from flask import Flask
from app import app

if __name__=='__main__':
    app.secret_key = 's3cr3t'
    app.run(host= '0.0.0.0', threaded=True, debug=True)